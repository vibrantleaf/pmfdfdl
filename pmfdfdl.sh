#!/usr/bin/env bash
mkdir -p ~/.local/share/fonts
podman run -d -i -t --replace --security-opt=no-new-privileges:true --pull always --name pmfdfdl \
  --dns=8.8.8.8 --dns=8.8.4.4 --dns=2001:4860:4860::8888 --dns=2001:4860:4860::8844 \
  --volume '/etc/localtime:/etc/localtime:ro' --memory 750m --runtime crun \
  registry.fedoraproject.org/fedora:latest /bin/bash
podman exec -it pmfdfdl /bin/bash -c 'dnf install -y --skip-unavailable \
  wget fontconfig cabextract xorg-x11-font-utils git \
  cascadia-fonts-all fontawesome-fonts-all jetbrains-mono-fonts mozilla-fira-mono-fonts \
  mozilla-fira-sans-fonts fira-code-fonts mozilla-zilla-slab-highlight-fonts \
  powerline-fonts adobe-source-\* google-android-emoji-fonts google-go-\* google-droid-sans-mono-font \
  google-noto-fonts google-carlito-fonts google-rubik-fonts google-droid-sans-fonts \
  google-droid-serif-fonts libreoffice-opensymbol-fonts opendyslexic-fonts twitter-twemoji-fonts  \
  liberation-fonts wine-fonts default-fonts levien-inconsolata-fonts powerline-fonts roboto-fontface-fonts \
  https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm && \
  mkdir -p /usr/share/fonts/ComicMonoNF/ /usr/share/fonts/adwaita-fonts-48.0 && \
  wget -O /usr/share/fonts/ComicMonoNF/ComicMonoBold-NF.ttf \
    https://github.com/vibrantleaf/comic-mono-font-NF/releases/download/2022-12-3/Comic.Mono.Bold.Nerd.Font.Complete.ttf && \
  wget -O /usr/share/fonts/ComicMonoNF/ComicMono-NF.ttf \
    https://github.com/vibrantleaf/comic-mono-font-NF/releases/download/2022-12-3/Comic.Mono.Nerd.Font.Complete.ttf && \
  wget -O /tmp/adwaita-fonts-48.0.tar.xz https://download.gnome.org/sources/adwaita-fonts/48/adwaita-fonts-48.0.tar.xz && \
  tar -xfv /tmp/adwaita-fonts-48.0.tar.xz -C /usr/share/fonts/adwaita-fonts-48.0 && \
  rm -v /tmp/adwaita-fonts-48.0.tar.xz && \
  git clone https://github.com/thelioncape/San-Francisco-family.git /tmp/San-Francisco-family && \
  rm -rfv /tmp/San-Francisco-family/{README.md,.git,index.html,CONTRIBUTING.md,website_resources,"TrueType versions"} && \
  mv -v /tmp/San-Francisco-family /usr/share/fonts/San-Francisco-family && \
  tar -czfv /myfonts.tar.gz /usr/share/fonts/'
podman start pmfdfdl
podman cp pmfdfdl:/myfonts.tar.gz ~/.cache
podman stop pmfdfdl
podman rm pmfdfdl
tar -tfv ~/myfonts.tar.gz -C ~/.cache/myfonts
sudo chown $(whoami):$(whoami) ~/.cache/myfonts
chmod 0644 ~/.cache/myfonts
cp -rfv ~/.cache/myfonts/usr/share/fonts/* ~/.local/share/fonts
sudo restorecon -RFv ~/.local/share/fonts
sudo rm -rfv ~/.cache/myfonts ~/.cache/myfonts/myfonts.tar.gz
fc-cache -fv